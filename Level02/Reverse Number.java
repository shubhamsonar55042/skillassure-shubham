/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program to reverse a given user number
import java.util.*;
public class Main
{
  public static void main (String[]args)
  {
    // Scanner for user Input
    Scanner sc = new Scanner (System.in);
      System.out.println ("Enter the number");
    int n = sc.nextInt ();
    int reverse = 0;
    // using while loop we reverse the number
    while (n != 0)
      {
	int remainder = n % 10;
	  reverse = reverse * 10 + remainder;
	  n = n / 10;
      }
    System.out.println ("The reverse String is:" + reverse);
  }

}
