/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Program to calculate fabonacci series upto n number
import java.util.*;
public class Main
{
  public static void main (String[]args)
  {
 //Declretion of input      
  int n =0, n1 = 0, n2 = 1;
   
   //Scanner to take user input  
    Scanner sc = new Scanner (System.in);
   
   //reading input from the user  
   System.out.println ("Enter the Number: ");
      n = sc.nextInt ();
  
    System.out.println("Fibonacci Series till " + n + " terms:");
   //for is used perform loop operation
    for (int i = 1; i <= n; ++i) {
      System.out.print( n1+ ",");

    // compute the next term
      int n3 = n1 + n2;
      n1 = n2;
      n2 = n3;
    }
  }
}
