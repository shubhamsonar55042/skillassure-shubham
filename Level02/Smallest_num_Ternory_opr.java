/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Program to find smallest of three number using ternary operation
import java.util.*;
public class Main
{
  public static void main (String[]args)
  {
  // Decleration of input
    int a, b, c, smallest, temp;
   
   //Scanner to take user input  
    Scanner sc = new Scanner (System.in);
   
   //reading input from the user  
      System.out.println ("Enter the first number:");
      a = sc.nextInt ();
      System.out.println ("Enter the second number:");
      b = sc.nextInt ();
      System.out.println ("Enter the third number:");
    
      c = sc.nextInt ();
    //comparing a and b and storing the smallest number in a temp variable  
      temp = a < b ? a : b;
   
    //comparing the temp variable with c and storing the result in the variable names smallest  
      smallest = c < temp ? c : temp;
   
    //prints the smallest number  
      System.out.println ("The smallest number is: " + smallest);
  }
}
