/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Program to find sum of elements of an array
import java.util.*;
public class Main
{
	public static void main(String[] args) {
	    //Decleration of Input
  int number = 0, originalNumber, remainder, result = 0;
  
  // Scanner is used to take user input
  Scanner sc= new Scanner(System.in);
  System.out.println("Enter the Number");
  number = sc.nextInt();

        originalNumber = number;
        
       //while loop is used to perform operatio if number is not 0
        while (originalNumber != 0)
        {
            remainder = originalNumber % 10;
            result += Math.pow(remainder, 3);
            originalNumber /= 10;
        }
        
        //If for print output acording to while loop
        if(result == number)
            System.out.println(number + " is an Armstrong number.");
        else
            System.out.println(number + " is not an Armstrong number.");
    }
} 
	
        

	

