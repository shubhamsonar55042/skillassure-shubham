/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program to print a prime number from 1 to 100
import java.util.*;
public class Main
{
  public static void main (String[]args)
  {
    int i=0;
    int num=0;
    String Prime="";
    // For loop to take number upto 100
    for(i=1;i<=100;i++){
       int counter=0;
        //
        for(num=i; num>=1; num--){
            if(i%num==0){
                counter=counter + 1;
            
            }
        }
        if (counter==2)
        {
            Prime = Prime + i +" ";
        }
    }
    System.out.println("Prime number from 1 to 100 are :"+ Prime);
    
  }

}
