/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program to write a series of 1, 2 , 3, 5, 8, 13....
import java.util.*;
public class Main
{
	public static void main(String[] args) {
	    // Decleration of input 
	      int n1=0,n2=1,n3;
	   
	    //Scanner is use to take user input
	      Scanner sc = new Scanner(System.in);
	   	  System.out.println("Enter the Number");
	
		// this count is for user input e.x:- n=10
	      int count = sc.nextInt();
	      for (int i=0;i<count;i++){
	        n3=n1+n2;
	        System.out.println(""+n3);
	        n1=n2;
	        n2=n3;
	   }
	
	}
}


