/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
//Program to Swapping two Numbers
public class Main
{
	public static void main(String[] args) {
    //Step1: Intialize the variable
    
    int a,b,c=0;
    
    //Step2: Declear the two variables
    a=10;
    b=20;
    System.out.println("Before Swapping: "+a+" "+b);
    
    //step3: swapping of Numbers
    c=a;
    a=b;
    b=c;
    System.out.println("After Swapping: "+a+" "+b);
    
	}
}