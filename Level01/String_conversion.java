/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program to write a series of 1, 2 , 3, 5, 8, 13....
import java.util.*;
public class Main
{
	public static void main(String[] args) 
	{
	  //Scanner is used to take input from user
      Scanner sc= new Scanner(System.in);
      System.out.println("Enter the String :");
      String str= sc.nextLine();// To take a Whole sentence
    
      //To join a char into String We use String Buffer
      StringBuffer newStr= new StringBuffer(str);
     
        // To take a char in loop we use for
         for(int i=0;i<str.length();i++)
        {
            // If else module use to for String Case conversion
            if(Character.isLowerCase(str.charAt(i)))
            {
              newStr.setCharAt(i,Character.toUpperCase(str.charAt(i)));
	        }
	         else if(Character.isUpperCase(str.charAt(i)))
	        {
	         newStr.setCharAt(i,Character.toLowerCase(str.charAt(i)));
	        }
	}
        System.out.println("String after case conversion :" +newStr);
      }
 }


