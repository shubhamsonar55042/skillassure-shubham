/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program to write a series of 4, 16, 36, 64
import java.util.*;
public class Main
{
	public static void main(String[] args) {
	    //Scanner is use to take user input
	   Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		// this n is for user input e.x:- n=10
		int n = sc.nextInt();
	   
	    // For looping purpose we use sign
		for( int i=2;i<=n;i++)
		{
	       //If is used for apply condition for even numbers
            if(i%2==0)
            System.out.println(i*i);
		}
	}
}

