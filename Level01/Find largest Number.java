/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
// Program for finding Biggest number
import java.util.*;
public class Main{
public static void main(String[] args) {
         //To take a input we use scanner
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the 3 numbers");
        int a= sc.nextInt();
        int b= sc.nextInt();
        int c= sc.nextInt();
        
        //if else chain use to give condition for greatest number 
        if(a>b && a>c){
            System.out.println("a is greater number");
        }else if(b>a && b>c){
            System.out.println("b is greater number");
        }else
        System.out.println("c is greater number");
             
        // This if else chain is used to give condition for 2nd largest number 
        if(a>b && a<c || a<b && a>c){
            System.out.println("a is 2nd greater number");
        }else if(a<b && c>b || a>b && b>c){
            System.out.println("b is  2nd greater number");
        }else
        System.out.println("c is 2nd greater number");
}
}





